#!/usr/bin/env /usr/bin/python
# -*- coding: utf-8 -*-
from sys import argv
from pyPdf import PdfFileReader
import re

if len(argv) < 2:	
    print("""This is pdfbib, used to directly export pdf information to bib format for use in latex.

    Usage:
    pdfbib /path/to/document.pdf /new/bib/file.bib""")
    raise SystemExit

#else:
infile = argv[1]
try:
    document=PdfFileReader(open(infile))
except IOError as (errno,errstr):
    print "Could not open file "\
	, infile, ". This is because, ", errstr

#get info and pages
info = document.getDocumentInfo()
meta = document.getXmpMetadata()
print info
#print dir(meta)
#pattern = '\b((?:[.][0-9]+)*/(?:(?!["&\'<>]))+)\b' #REGEX not working.
#pattern = """\b(10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?!["&\'])\S)+)\b"""
#from https://doeidoei.wordpress.com/2009/10/22/regular-expression-to-match-a-doi-digital-object-identifier/
#pattern = re.compile('[doi|DOI][\s\.\:]{0,2}(10\.\d{4}[\d\:\.\-\/a-z]+)[A-Z\s]')
pattern="""10\.\d{4,9}/[-._;()/:A-Za-z0-9]+$"""
re.UNICODE
regex=re.compile(pattern)

for page in document.pages:
    text = page.extractText()
#    print regex.findall(page.extractText())  #regex.search(page)
    print regex.findall(text)

